# Databricks notebook source
# MAGIC %run "../includes/configuration"  --create file an assign variables and those variables become part of this program, "configuration" is name of notebook
# MAGIC 
# MAGIC import sys
# MAGIC import pandas as pd
# MAGIC 
# MAGIC print("Loading movie ratings...")
# MAGIC exit #need import sys to work
# MAGIC sys.exit()
# MAGIC movieID_to_name = {}
# MAGIC name_to_movieID = {}
# MAGIC --pandas read
# MAGIC ratingsPath = '../ml-latest-small/ratings.csv'
# MAGIC moviesPath = '../ml-latest-small/movies.csv'
# MAGIC 
# MAGIC #Using Spark (different than pandas; absolute location needed)
# MAGIC from pyspark.sql import SparkSession
# MAGIC from pyspark.sql import Row
# MAGIC from pyspark.sql import functions as func
# MAGIC import sys
# MAGIC spark = SparkSession.builder.appName("FriendsByAge").getOrCreate()
# MAGIC 
# MAGIC #lines = spark.read.option("header", "true").option("inferSchema", "true").csv("dbfs:/mnt/taming/db/fakefriends/fakefriends-header.csv") --won't work; remove dbfs:
# MAGIC #lines = spark.read.option("header", "true").option("inferSchema", "true").csv("file:/Workspace/Repos/Projects/Video-TamingBigDatawithapacheSparkandPython/fakefriends.csv")
# MAGIC 
# MAGIC 
# MAGIC lines = spark.read.option("header", "true").option("inferSchema", "true").csv(path_of_spark_file)
# MAGIC lines = spark.read.option("header", "true").option("inferSchema", "true").csv(f"{path_of_spark_file}fakefriends.csv")
# MAGIC 
# MAGIC display(lines);
# MAGIC sys.exit("Code stopped program")
# MAGIC 
# MAGIC 
# MAGIC 
# MAGIC ratingsDataset = pd.read_csv('../ml-latest-small/ratings.csv')
# MAGIC 
# MAGIC display(ratingsDataset);
# MAGIC print("another comment");
# MAGIC 
# MAGIC 
# MAGIC #ratingsDataset

# COMMAND ----------



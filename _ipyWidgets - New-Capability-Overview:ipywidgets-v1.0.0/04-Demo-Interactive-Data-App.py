# Databricks notebook source
# MAGIC %md-sandbox
# MAGIC 
# MAGIC <div style="text-align: center; line-height: 0; padding-top: 9px;">
# MAGIC   <img src="https://databricks.com/wp-content/uploads/2018/03/db-academy-rgb-1200px.png" alt="Databricks Learning" style="width: 600px">
# MAGIC </div>

# COMMAND ----------

# MAGIC %md
# MAGIC # Interactive Data App

# COMMAND ----------

# MAGIC %md
# MAGIC In this notebook, we will build a simple flight search app using widgets. While building the app we will follow these steps;
# MAGIC * Read `flights` dataset which has `origin`, `destination`, `distance` and `delayed` fields.
# MAGIC * Fill widget values from the dataset. *Origin* and *destination* dropdown lists will list unique values of corresponding fields. Similarly, we will use a slider for *distance* value and the minimum and maximum values of the slider will be extracted from the dataset. 
# MAGIC * When we click on the Search button, data will be filtered based on the selected widget values.
# MAGIC * Filtered dataset will be printed.
# MAGIC 
# MAGIC 
# MAGIC **Requirements:**
# MAGIC * This notebook requires a cluster with **DBR version 11.0** or above.
# MAGIC * This notebook requires a cluster with **DBR version 12.1** or above for **Unity Catalog (UC) managed workspaces**.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Install Datasets
# MAGIC 
# MAGIC The first thing we're going to do is to run a setup script. This script will define the required configuration variables that are scoped to each user.
# MAGIC 
# MAGIC Next, we need to "install" the datasets this course uses by copying them from their current location in the cloud to a location relative to your workspace. 
# MAGIC 
# MAGIC In this course, we will be using one of the datasets provided by Databricks.
# MAGIC 
# MAGIC All that is required is to run the following cell.

# COMMAND ----------

# MAGIC %run ./Includes/Classroom-Setup-04

# COMMAND ----------

#Install datasets
DA.install_datasets(reinstall=False)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Load Dataset
# MAGIC 
# MAGIC In this step, we will use Spark to read `.csv` file and do some basic transformations. Then, a list of origin and distance airports is extracted from the dataset using distinct values of each field in the dataset. Finally, maximum and minimum values of the distance column are retreieved from the dataset.
# MAGIC 
# MAGIC These variables will be used to populate widgets' selection options.

# COMMAND ----------

from pyspark.sql.functions import col, when, to_date

DA.paths.flights_dataset = f"dbfs:/mnt/dbacademy-datasets/{DA.data_source_name}/{DA.data_source_version}/flights/flights-01.csv"

df = spark.read.csv(DA.paths.flights_dataset, header="true", inferSchema="true")

df = df.withColumns({
    'delayed': (when(col('delay').cast('int') > 0, "Yes").otherwise("No"))
})

origin_list = [r.origin for r in df.select(['origin']).distinct().collect()]
destination_list = [r.destination for r in df.select(['destination']).distinct().collect()]
max_distance = df.selectExpr('max(INT(distance))').take(1)[0][0]
min_distance = df.selectExpr('min(INT(distance))').take(1)[0][0]


# COMMAND ----------

# MAGIC %md
# MAGIC ## Build *Flight Search* App
# MAGIC 
# MAGIC **Data Collection Widgets:**
# MAGIC 
# MAGIC In this simple data app, we will use various widgets to demonstrate their usage. We will use dropdown, slider, radio button, and button widgets to collect data from the users. 
# MAGIC 
# MAGIC **Output Widget:**
# MAGIC 
# MAGIC `Output` widget is used to update the output when users click on the search button.
# MAGIC 
# MAGIC 📌 Note: The `Output` widget does not support displaying Spark data frames. Thus, we will use the `toPandas` method to convert it to Pandas data frame and display it with the `IPython.display` method instead of Databricks notebooks' default display method.
# MAGIC 
# MAGIC 
# MAGIC **Layout Widgets:**
# MAGIC 
# MAGIC For managing the layout, we will use `AppLayout` and `VBox` widgets. `AppLayout` is a widget layout template that allows you to create an application-like widget arrangement. It consists of a header, a footer, two sidebars, and a central pane. `VBox` widget is a helper widget that provides a simple way to arrange child widgets in a vertical box. 

# COMMAND ----------

import ipywidgets as widgets
from ipywidgets import Output, Button, Box, VBox, HBox, HTML, IntRangeSlider, Dropdown, RadioButtons, AppLayout, Layout
from IPython.display import display

# define Output widget that we will use the control the output of the app
output = Output()

# This method is called by on_click event of the search button. Data is filtered based on selected widget values and filtered data is shown 
def search_flights(b):
    output.clear_output()
    with output:
        search_result = df.where(f"origin == '{origin.value}' AND destination == '{dest.value}' AND delayed == '{delayed.value}' AND distance > {distance.value[0]} AND distance < {distance.value[1]} ")
        if len(search_result.head(1)) > 0:
            display(search_result.toPandas())
        else:
            print("No flights found.")
            
        
# Define widgets 
title = HTML('<h2>Flight Search App:</h2>')

origin = Dropdown(
    description = 'Origin: ',
    options = origin_list
)

dest = Dropdown(
    description = 'Destination: ',
    options = destination_list
)

distance = IntRangeSlider(
    description = 'Distance: ',
    min = min_distance,
    max = max_distance
)

delayed = RadioButtons(
    description = 'Delayed? ',
    options = ['No', 'Yes']
)

search_btn = Button(
    description = "Search Flights",
    button_style = 'info',
    layout = Layout(margin = 'auto 20px')
)
search_btn.on_click(search_flights) 

# Divide widgets into two columns 
left_col = VBox(children = [origin, dest], layout = Layout(height = 'fit-content'))

right_col = VBox(children = [distance, delayed], layout = Layout(height = 'fit-content'))

# Use AppLayout widget using custom size and layout options
app = AppLayout(
    header = title,
    left_sidebar = left_col,
    center = None,
    right_sidebar = right_col,
    footer = search_btn,
    width = '750px',
    pane_heights = ['60px', '1fr', '60px'],
    layout = Layout(border = '5px solid #ddd', padding = '10px'),
    style = ''
)

# Display final state of widgets
display(app, output)


# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Clean up Classroom
# MAGIC 
# MAGIC Run the following cell to remove lessons-specific assets created during this lesson:

# COMMAND ----------

DA.cleanup()

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC &copy; 2022 Databricks, Inc. All rights reserved.<br/>
# MAGIC Apache, Apache Spark, Spark and the Spark logo are trademarks of the <a href="https://www.apache.org/">Apache Software Foundation</a>.<br/>
# MAGIC <br/>
# MAGIC <a href="https://databricks.com/privacy-policy">Privacy Policy</a> | <a href="https://databricks.com/terms-of-use">Terms of Use</a> | <a href="https://help.databricks.com/">Support</a>

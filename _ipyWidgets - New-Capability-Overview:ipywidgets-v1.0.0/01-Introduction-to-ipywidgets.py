# Databricks notebook source
# MAGIC %md-sandbox
# MAGIC 
# MAGIC <div style="text-align: center; line-height: 0; padding-top: 9px;">
# MAGIC   <img src="https://databricks.com/wp-content/uploads/2018/03/db-academy-rgb-1200px.png" alt="Databricks Learning" style="width: 600px">
# MAGIC </div>

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Introduction to `ipywidgets`
# MAGIC Until recently, Databricks widgets have been the only option for users wanting to use widgets in Databricks notebooks. Now, with the integration of ipywidgets, Databricks users have an alternative method for building interactive notebooks. 
# MAGIC 
# MAGIC Widgets can be embedded into the Databricks notebooks to provide a user-friendly interface that can be used to collect data and use it in the code without having to edit the code. This method can be used to create interactive data apps in notebooks. 
# MAGIC 
# MAGIC This walkthrough demonstration will introduce you to the `ipywidgets` library and show how to use this library in Databricks notebooks.  
# MAGIC 
# MAGIC 
# MAGIC **Learning Objectives**
# MAGIC 1. Display widgets from the ipywidgets library
# MAGIC 1. Utilize ipywidgets controls to create interactive notebooks
# MAGIC 1. Use layout and styling controls to build custom layouts
# MAGIC 
# MAGIC 
# MAGIC **Requirements:**
# MAGIC * This notebook requires a cluster with **DBR version 11.0** or above.
# MAGIC 
# MAGIC **Documentation Note:**
# MAGIC * In the notebooks you will see `ipywidgets` documentation links. Please make sure to check the documentation version used by Databricks. Simply run the code cell below to see the version and documentation link.

# COMMAND ----------

# MAGIC %md
# MAGIC ---

# COMMAND ----------

import ipywidgets as widgets

print(f"ipywidgets version: {widgets.__version__}")

print(f"ipywidgets documentation: https://ipywidgets.readthedocs.io/en/{widgets.__version__}")

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Display a Simple Widget
# MAGIC 
# MAGIC The first step we need to do is to import the `ipywigets` library. Then we can define the widget we want and display it.
# MAGIC 
# MAGIC Widgets might be displayed using these two methods;
# MAGIC * **Instantly**: Defining or invoking a widget at the end of a cell will automatically display it inside the output area of the cell.  
# MAGIC * **Manually**: Explicitly displaying the widget using the `display` method. The same widget can be displayed multiple times inside a notebook and the state of the widget remains in sync among all instances. 
# MAGIC 
# MAGIC In this example, we will create a numeric slider controller but in the following sections we will cover various widget types that you can use.
# MAGIC 
# MAGIC *Note: `display` method should be called first, which means it should be called before cell output renders. Otherwise, the widget will not be displayed.*

# COMMAND ----------

import ipywidgets as widgets                #import ipywidgets library

slider_1 = widgets.FloatSlider()            #
display(slider_1)                           # display manually

widgets.IntSlider()                         # display instantly


# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC Now let's display the same widget we just created and test if they are in sync.

# COMMAND ----------

slider_1

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Widget Properties
# MAGIC 
# MAGIC Widgets have properties that can be defined to control the visual representation and behavior of the widget. Some of the most common widget properties are:
# MAGIC * Label properties: `description`
# MAGIC * Visual display properties: `disabled`, `orientation`, `style`
# MAGIC * Value properties: `value`, `min`, `max`, `step`
# MAGIC 
# MAGIC For a supported list of properties for each widget, please refer to the `ipywidgets` <a href="https://ipywidgets.readthedocs.io/en/7.7.0/index.html" target="_blank">official documentation</a>.   
# MAGIC 
# MAGIC 
# MAGIC Let's use some of the properties to create a custom numeric range slider widget.

# COMMAND ----------

widgets.IntRangeSlider(             
    value = [4, 16],               # initial range value
    min = 1,                       # minimum value 
    max = 20,
    step = 2,
    description = "Select Range:",
    disabled = False
)

# COMMAND ----------

# MAGIC %md
# MAGIC ---

# COMMAND ----------

# MAGIC %md
# MAGIC ## Create Widgets Automatically
# MAGIC 
# MAGIC In the previous section, we created widgets manually. To simplify the process, `ipywidgets` library provides the **`interact`** function which autogenerates UI controls for function arguments and then calls the function with those arguments when you manipulate the controls interactively. To use `interact`, we need to define a function and pass it along with default argument values into the `interact` function.
# MAGIC 
# MAGIC ### Basic `interact` example
# MAGIC 
# MAGIC To see how the `interact` function works, let's create a function that takes an integer and returns the square of the input argument. Then we will create an autogenerated widget that will create a UI control and keeps it updated as the value changes. The `interact` function shows the appropriate control based on the initial value that we pass. In this example, we are passing a numeric value, therefore `interact` autogenerates a numeric slider for us.

# COMMAND ----------

from ipywidgets import interact

def square(x):
    return f"Square of {x} is {x*x}"

interact(square, x=10)

# COMMAND ----------

# MAGIC %md
# MAGIC ### `interact` decorator
# MAGIC 
# MAGIC Interact method can also be used as python decorator. With a decorator, we can define a function and let the `ipywidget` library create widgets based on the function arguments in a single shot. Let's update the previous example and use a decorator. As the `interact` function accepts more than one argument, we will update the square function to take two numeric values; a base and an exponent value.

# COMMAND ----------

@interact(base=10, exponent=3)
def power_of(base, exponent):
    return pow(base,exponent) 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Customize widgets with `interact`
# MAGIC 
# MAGIC Although using the default `interact` functionality is great, sometimes we need to customize the widgets' properties. In the previous example, we don't have control over how widgets are displayed. Let's modify this example and control how the widgets are displayed. For the *base*, we will use an integer slider with a custom `min`, `max`, default `value` and label text. Similarly, for the exponent value, we will display it as a textbox. As shown in this example, we have full control over the way the `interact` function displays autogenerated widgets.

# COMMAND ----------

@interact(
    base=widgets.IntSlider(min=1, max=100, value=20, description="Base: "), 
    exponent=widgets.IntText(min=1, max=10, value=2, description="Exponent: ")
)
def power_of(base, exponent):
    return pow(base,exponent)

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC In addition to the `interact` function, `ipywidgets` has a couple of other functions that may be helpful in some cases.
# MAGIC * `interactive`: Useful when you want to reuse the widget. Unlike `interact`, this function will not display widgets automatically which means you can display them when you wish to.
# MAGIC * `interact_manual`: This function is useful when you need to update the control on demand and prevent auto update.
# MAGIC * `interactive_output`: With this function, you can control how the UI elements are laid out. This is powerful as you can customize the widget and its layout.
# MAGIC 
# MAGIC These functions are not covered in this lesson. For more information about these functions and examples please refer to <a href="https://ipywidgets.readthedocs.io/en/7.7.0/examples/Using%20Interact.html" target="_blank">ipywidgets library's documentation</a>. 

# COMMAND ----------

# MAGIC %md
# MAGIC ## Summary
# MAGIC 
# MAGIC In this section of the course, we first discussed how to import the `ipywidgets` library and showed how to display a simple widget.
# MAGIC 
# MAGIC Next, we covered widget properties and their usage.
# MAGIC 
# MAGIC In the final part of the lesson, we used a convenient method of displaying widgets. We used the `interact` method and `interact` decorator to display autogenerated widgets  and discussed how to customize autogenerated widgets.   

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC &copy; 2022 Databricks, Inc. All rights reserved.<br/>
# MAGIC Apache, Apache Spark, Spark and the Spark logo are trademarks of the <a href="https://www.apache.org/">Apache Software Foundation</a>.<br/>
# MAGIC <br/>
# MAGIC <a href="https://databricks.com/privacy-policy">Privacy Policy</a> | <a href="https://databricks.com/terms-of-use">Terms of Use</a> | <a href="https://help.databricks.com/">Support</a>

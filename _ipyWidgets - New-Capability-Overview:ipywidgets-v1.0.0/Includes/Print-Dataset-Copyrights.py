# Databricks notebook source
# MAGIC %run ./_utility-functions

# COMMAND ----------

def print_copyrights(self):
    datasets = [f.path for f in dbutils.fs.ls(DA.hidden.datasets)]
    for dataset in datasets:
        readme_path = f"{dataset}README.md"
        try:
            head = dbutils.fs.head(readme_path)
            lines = len(head.split("\n"))+1
            html = f"""<html><body><h1>{dataset}</h1><textarea rows="{lines}" style="width:100%; overflow-x:scroll">{head}</textarea></body></html>"""
            displayHTML(html)
        except:
            print(f"\nMISSING: {readme_path}")
        
DBAcademyHelper.monkey_patch(print_copyrights)

# COMMAND ----------

DA = DBAcademyHelper()
DA.init(create_db=False)

# COMMAND ----------

DA.print_copyrights()

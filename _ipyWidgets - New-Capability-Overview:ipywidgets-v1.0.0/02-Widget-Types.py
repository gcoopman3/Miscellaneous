# Databricks notebook source
# MAGIC %md-sandbox
# MAGIC 
# MAGIC <div style="text-align: center; line-height: 0; padding-top: 9px;">
# MAGIC   <img src="https://databricks.com/wp-content/uploads/2018/03/db-academy-rgb-1200px.png" alt="Databricks Learning" style="width: 600px">
# MAGIC </div>

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Widget Types
# MAGIC 
# MAGIC `ipywidgets` library supports a wide range of widgets. Although we are not going to cover all of them, in this lesson we will cover the most common widgets. 
# MAGIC 
# MAGIC **Learning Objectives:**
# MAGIC * Define widgets from the `ipywidgets` library
# MAGIC * Utilize ipywidgets controls to create interactive notebooks
# MAGIC * Use layout and styling controls to build custom layouts
# MAGIC 
# MAGIC **Requirements:**
# MAGIC * This notebook requires a cluster with DBR version 11.0 or above.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Numeric Widgets
# MAGIC 
# MAGIC Numeric widgets are designed to display numeric values. They can show integer and float values in the form of slider, textbox, and progress bars. Let's look at each of these widget types.

# COMMAND ----------

# MAGIC %md
# MAGIC ### Numeric Sliders
# MAGIC 
# MAGIC Available widgets: `IntSlider`, `FloatSlider`, `FloatLogSlider`

# COMMAND ----------

# Int Slider
import ipywidgets as widgets

widgets.IntSlider(
    value=7,
    min=0,
    max=10,
    step=1,
    description='Int Slider:',
    disabled=False,
    continuous_update=False,
    orientation='horizontal',
    readout=True,
    readout_format='d'
)

# COMMAND ----------

# Float slider
import ipywidgets as widgets

widgets.FloatSlider(
    value=5.5,
    min=0,
    max=10.0,
    step=0.1,
    description='Float Slider:',
    disabled=False,
    continuous_update=False,
    orientation='horizontal',
    readout=True,
    readout_format='.1f'
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Numeric Range Sliders
# MAGIC 
# MAGIC Available widgets: `IntRangeSlider`, `FloatRangeSlider`

# COMMAND ----------

# Int Range Slider
import ipywidgets as widgets

widgets.IntRangeSlider(
    value=[20, 50],
    min=0,
    max=100,
    step=1,
    description='Int Range:',
    disabled=False,
    continuous_update=False,
    orientation='horizontal',
    readout=True,
    readout_format='d',
)

# COMMAND ----------

# Int Range Slider
import ipywidgets as widgets

widgets.FloatRangeSlider(
    value=[10.2, 30.5],
    min=0,
    max=100.0,
    step=0.1,
    description='Float Range:',
    disabled=False,
    continuous_update=False,
    orientation='horizontal',
    readout=True,
    readout_format='.1f',
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Progress Bars
# MAGIC 
# MAGIC Available widgets: `IntProgress`, `FloatProgress`

# COMMAND ----------

# Int Progress Bar
import ipywidgets as widgets

widgets.IntProgress(
    value=30,
    min=0,
    max=100,
    step=1,
    description='Progress:',
    bar_style='info',
    orientation='horizontal'
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Numeric Textbox
# MAGIC 
# MAGIC Available widgets: `BoundedIntText`, `BoundedFloatText`, `IntText`, `FloatText`

# COMMAND ----------

# Int Bounded Text
import ipywidgets as widgets

widgets.BoundedIntText(
    value=65,
    min=0,
    max=100,
    step=1,
    description='Value:',
    disabled=False
)


# COMMAND ----------

# MAGIC %md
# MAGIC ## Boolean Widgets
# MAGIC 
# MAGIC Boolean widgets are designed to toggle states. 
# MAGIC 
# MAGIC Available widgets: `ToggleButton`, `Checkbox`, `Valid`

# COMMAND ----------

# Checkbox
import ipywidgets as widgets

widgets.Checkbox(
    value=False,
    description='Do like this?',
    disabled=False
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Selection Widgets
# MAGIC 
# MAGIC Selection widgets allow you to show a list of options and select an option from the list. 
# MAGIC 
# MAGIC Available widgets: `Dropdown`, `RadioButtons`, `Select`, `SelectionSlider`, `SelectionRangeSlider`, `Toggle Buttons`, `SelectMultiple`

# COMMAND ----------

# Dropdown widget

import ipywidgets as widgets

widgets.Dropdown(
    options=[('X-Small', 1), ('Small', 2), ('Medium', 3), ('Large', 4), ('X-Large', 5)],
    value=3,
    description='Size:',
)

# COMMAND ----------

import ipywidgets as widgets

widgets.ToggleButtons(
    options=['Small', 'Medium', 'Large'],
    description='Size:',
    disabled=False,
    button_style='success', # 'success', 'info', 'warning', 'danger' or ''
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## String Widgets
# MAGIC 
# MAGIC There are several controls that are provided by `ipywidgets` library to display string values. 
# MAGIC 
# MAGIC Available widgets: `Text`, `Textarea`, `Label`, `HTML`, `HTMLMath`

# COMMAND ----------

# Text widget

import ipywidgets as widgets

widgets.Text(
    value='',
    placeholder='Enter Name',
    description='Name:',
    disabled=False
)

# COMMAND ----------

# HTML

import ipywidgets as widgets

widgets.HTML(
    value='<h3>HTML Output:</h3><p>Paragraph 1</p><p>Paragraph 2</p><p>Paragraph 3</p><p>Can even display <a href="#">links</a></p>',
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Date and Color Pickers
# MAGIC 
# MAGIC These controls allow you to select a date or color using user-friendly UI.

# COMMAND ----------

# Date Picker
import ipywidgets as widgets
widgets.DatePicker(
    description='Pick a Date',
    disabled=False
)

# COMMAND ----------

#Color Picker

import ipywidgets as widgets

widgets.ColorPicker(
    concise=False,
    description='Pick a color',
    value='#3d3daa',
    disabled=False
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Container and Layout Widgets
# MAGIC 
# MAGIC These widgets' primary function is to organize other widgets and create custom layouts. 
# MAGIC 
# MAGIC Available widgets: `Box`, `HBox`, `VBox`, `GridBox`, `Accordion`, `Tabs`
# MAGIC 
# MAGIC One of the common container widgets is the accordion. In this example, we will create an accordion widget and set title labels for each section. 

# COMMAND ----------

import ipywidgets as widgets
accordion = widgets.Accordion(children=[widgets.IntSlider(), widgets.Text()])
accordion.set_title(0, 'Slider')
accordion.set_title(1, 'Text')
accordion

# COMMAND ----------

# MAGIC %md
# MAGIC ## Events and Output Widget
# MAGIC 
# MAGIC In this section, we will cover two `ipywidget` components which are related to each other; widget events and the `Output` widget. 
# MAGIC 
# MAGIC Events are used to handle a user action, such as a button click or a slider change. For more information about different widget event types, please refer to the `ipywidgets` <a href="https://ipywidgets.readthedocs.io/en/7.7.0/examples/Widget%20Events.html" target="_blank">official documentation</a>.
# MAGIC 
# MAGIC The `Output` widget is a special widget designed to capture and display rich output generated by code block, `stdout` and `stderr`. As we will demonstrate below, when an event is fired, we will update the output to display a message or display another widget to the user. The `Output` widget helps us to manipulate the output by adding or removing the content from the output.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Events Example - Without Output Widget
# MAGIC 
# MAGIC Let's start by using a simple `on_click` event with the button widget. First, we need to define a function and then we need to bind that function to the widget event that we want to handle. In this example, we defined the `clicked` function and then assigned it to the button's `on_click` event.
# MAGIC 
# MAGIC 💡 Note: In this example, we are not going to see the printed message as the output is not updated.   

# COMMAND ----------

button = widgets.Button(description="Click Me")

def clicked(b):
    print("User clicked the button. This message is not gonna show up!")

button.on_click(clicked)

display(button)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Events Example - With Output Widget
# MAGIC 
# MAGIC Let's update the previous example and use the `Output` widget to display a text message and a widget. 
# MAGIC 
# MAGIC As it is shown in this example, the `Output` widget supports displaying not only text output but also additional widgets. In addition, the widget has additional methods to append display data. 
# MAGIC 
# MAGIC In some cases, we may need to clear the output of the widget. The `clear_output` method allows us to reset the output. 

# COMMAND ----------

button = widgets.Button(description="Click Me")
output = widgets.Output()
slider = widgets.IntSlider()

def clicked(b):
    
    # printing message without the output widget
    print("This message is not passed to output...")
    
    # reset the output with each click
    output.clear_output()
    
    with output:
        #display text message
        print("User clicked the button. Yay!")
        
        #display another widget
        display(slider)
        
button.on_click(clicked)

# we can append to output widget
append_txt = widgets.HTML('<p>We can append widgets to the <em>Output</em> widget.</p>')
output.append_display_data(append_txt)

display(button, output)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Debugging Errors With Output Widget
# MAGIC 
# MAGIC As we have demonstrated above, output generated by widget callbacks are not displayed anywhere and we used the `Output` widget to overcome this problem.
# MAGIC 
# MAGIC Without the output widget, if the code in the callback function has any error, we will not be able to see it. In this case, the `Output` widget helps to debug errors as the output will display the traceback of the error. 

# COMMAND ----------

output = widgets.Output()
button = widgets.Button(description="Click Me")
slider = widgets.IntSlider(value=50, min=0, max=100)

def callback(event):
    # raises en error but we don't see it and it blocks the rest
    #print(100/0)
    
    # with output widget, we error message is shown  
    with output:
        print(100/slider.value)

button.on_click(callback)
display(slider, button, output)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Summary
# MAGIC 
# MAGIC `ipywidgets` library provides a wide range of controls that can be used to create interactive data applications inside notebooks. In this lesson, we covered some of the commonly used widgets. In addition, we demonstrated how to use widget events and how to update the output with `Output` widget.
# MAGIC 
# MAGIC A final note; Databricks supports most of the widgets provided by the `ipywidgets` open-source library. Some of the widgets such as `Password`, `FileUpload`, and `Controller` are not supported.

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC &copy; 2022 Databricks, Inc. All rights reserved.<br/>
# MAGIC Apache, Apache Spark, Spark and the Spark logo are trademarks of the <a href="https://www.apache.org/">Apache Software Foundation</a>.<br/>
# MAGIC <br/>
# MAGIC <a href="https://databricks.com/privacy-policy">Privacy Policy</a> | <a href="https://databricks.com/terms-of-use">Terms of Use</a> | <a href="https://help.databricks.com/">Support</a>

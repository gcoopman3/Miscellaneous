# Databricks notebook source
# MAGIC %md-sandbox
# MAGIC 
# MAGIC <div style="text-align: center; line-height: 0; padding-top: 9px;">
# MAGIC   <img src="https://databricks.com/wp-content/uploads/2018/03/db-academy-rgb-1200px.png" alt="Databricks Learning" style="width: 600px">
# MAGIC </div>

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Layout and Styling of Widgets
# MAGIC 
# MAGIC In this notebook, we will present custom layout and styling options of the `ipywidgets` library. These options allow us to build custom user interfaces to build rich and interactive widget-based applications.
# MAGIC 
# MAGIC **Requirements:**
# MAGIC * This notebook requires a cluster with DBR version 11.0 or above.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Custom Layout
# MAGIC 
# MAGIC To create custom widget layouts, we will use the `layout` attribute of widgets. The `layout` attribute provides a convenient way of passing CSS properties to the widget controls. 
# MAGIC 
# MAGIC `ipwidgets` layout properties are mapped to the CSS properties with the same name. The only difference between CSS properties and the `ipywidgets` layout properties is that **dashes in CSS properties are replaced with underscores**. For example, CSS `min-height` property will be defined as `min_height` in `layout` properties. 
# MAGIC 
# MAGIC 📝 Please note that not all CSS properties are supported by the library. For a full list of supported properties and how to use shorthand CSS properties check the <a href="https://ipywidgets.readthedocs.io/en/7.7.0/examples/Widget%20Styling.html" target="_blank">official documentation</a>.  

# COMMAND ----------

# MAGIC %md
# MAGIC ### Example

# COMMAND ----------

# Int Slider
import ipywidgets as widgets

widgets.Button(
    description = 'Button with custom style',
    layout = widgets.Layout(
        width='200px', 
        height='60px', 
        padding='10px',
        border='2px dashed #69f'
    )
)

# COMMAND ----------

# MAGIC %md
# MAGIC ---

# COMMAND ----------

# MAGIC %md
# MAGIC ## Using Predefined Styles
# MAGIC 
# MAGIC As shown in the previous example, it is possible to define custom layouts for widgets. However, not all CSS properties are exposed by the `ipywidgets` library. Also, **the `layout` attribute only exposes layout-related CSS properties**. In other words, non-layout related properties such as `background-color` and `font-weight` properties are not exposed. If we want to change the background color of the button, we can use predefined styles. These are the styles defined by the environment and can be used by end-users. Many widgets comes with a list of pre-defined styles that user can use.
# MAGIC 
# MAGIC For example, the `Button` widget has the `button_style` attribute which allows us to select a style that has a custom predefined text and background color style.  

# COMMAND ----------

# MAGIC %md
# MAGIC ### Example

# COMMAND ----------

widgets.Button(
    description = 'Button with Warning Style', 
    button_style = 'warning', # use predefined style
    layout = widgets.Layout(width = 'auto', height = 'auto', padding = '5px 15px')
)

# COMMAND ----------

# MAGIC %md
# MAGIC ---

# COMMAND ----------

# MAGIC %md
# MAGIC ## Custom Styles
# MAGIC 
# MAGIC So far we discussed how to define custom layouts and it was noted that **the `layout` attribute only exposes layout-related CSS properties**. Another important widget attribute is **the `style` attribute which is used to expose non-layout-related styling attributes of widgets**. 
# MAGIC 
# MAGIC <b>💡</b> Please note that the properties of the `style` attribute are specific to each widget type. A list of style attributes can be retrieved with the `keys` property. 
# MAGIC 
# MAGIC In the next example, we will get a list of style attributes for the slider widget and define a custom style for handle color.

# COMMAND ----------

int_slider = widgets.IntSlider()
int_slider.style.keys

# COMMAND ----------

int_slider.style.handle_color = '#6666ff'
int_slider

# COMMAND ----------

# MAGIC %md
# MAGIC Another alternative method of defining custom styles for widgets is to pass style properties when a widget is constructed. Let's look at an example of using custom styles.
# MAGIC 
# MAGIC 📝 *Note: Some of the style properties such as `text_color` and `font_size` are not supported by Databricks.* 

# COMMAND ----------

widgets.Button (
    description = 'Button with Custom Style',
    style = dict(
        font_weight = 'bold',
        button_color = '#6666ff',
        text_decoration = 'underline',
        font_size = '20px'
    ),
     layout = widgets.Layout(width = 'auto', height = 'auto', padding = '5px 15px')
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Summary
# MAGIC 
# MAGIC `ipywidgets` library supports custom layout and styling for widgets. In this notebook, we first looked at the custom `layout` properties and used some of these properties to change the layout of the widgets. Also, we discussed how CSS properties are exposed as layout properties.
# MAGIC 
# MAGIC Next, we looked at the custom styling options. The first styling option is using widget specific pre-defined custom styles. The second option is using the `style` attribute which can be used te define custom style properties.
# MAGIC 
# MAGIC Custom layout and styles allow us to create interactive widget-based data applications. For more information about the custom layout and styling options of the `ipywidgets` library, please check the <a href="https://ipywidgets.readthedocs.io/en/7.7.0/examples/Widget%20Styling.html" target="_blank">official documentation</a>. 

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC &copy; 2022 Databricks, Inc. All rights reserved.<br/>
# MAGIC Apache, Apache Spark, Spark and the Spark logo are trademarks of the <a href="https://www.apache.org/">Apache Software Foundation</a>.<br/>
# MAGIC <br/>
# MAGIC <a href="https://databricks.com/privacy-policy">Privacy Policy</a> | <a href="https://databricks.com/terms-of-use">Terms of Use</a> | <a href="https://help.databricks.com/">Support</a>

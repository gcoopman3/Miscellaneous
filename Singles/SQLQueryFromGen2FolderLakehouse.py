# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ###SQL Query From Azure Data Lake Gen 2 Files
# MAGIC 
# MAGIC This notebook shows you how to create and query a table or DataFrame loaded from data stored in Azure Data Lake Gen 2.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Step 1: Connect to Storage Account

# COMMAND ----------

storage_account_name = "datalakehousegc"
storage_account_access_key = "DzyOTDfpEmCSciQky/7eHUHl7EWre9Pgebx1CuiWMXYSMtzJz0uqPV4Il94V2tkuuvZbGVOyrJKF+AStBjS/Ug=="


# COMMAND ----------

# establishing an execution context
spark.conf.set(
  "fs.azure.account.key."+storage_account_name+".dfs.core.windows.net",
  storage_account_access_key)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Step 2: Establish Folder Information

# COMMAND ----------

storage_type = "abfss:/"
file_container = "/cbronze"


file_loadpath = storage_type + file_container + "@" + storage_account_name + ".dfs.core.windows.net" 
#+ file_folderlocation + file_loadpath
file_loadpath

# COMMAND ----------

# MAGIC %sql
# MAGIC create database aaaa

# COMMAND ----------

# MAGIC 
# MAGIC %sql
# MAGIC create  database Bronze
# MAGIC location '/mnt/datalakehousecbronze2'
# MAGIC --'abfss://cbronze@datalakehousegc.dfs.core.windows.net'
# MAGIC 
# MAGIC --'/mnt/csv_files'

# COMMAND ----------

# MAGIC %sql
# MAGIC use Bronze

# COMMAND ----------

# MAGIC %sql
# MAGIC create table testgc(id int)

# COMMAND ----------

# MAGIC %sql
# MAGIC create table testgc2(id int)
# MAGIC location '/mnt/datalakehousecbronze2/testgc2'

# COMMAND ----------

# MAGIC %sql
# MAGIC describe extended testgc2

# COMMAND ----------

# MAGIC %sql
# MAGIC describe extended testgc

# COMMAND ----------

# MAGIC %sql
# MAGIC insert into bronze.testgc2(id)
# MAGIC select 2;
# MAGIC 
# MAGIC select * from testgc2

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from bronze.testgc2

# COMMAND ----------

# MAGIC %sql
# MAGIC show tables

# COMMAND ----------

sql
insert into bronze.testgc(id)
select 2;

select * from testgc


# COMMAND ----------

# MAGIC %sql
# MAGIC select * from bronze.testgc

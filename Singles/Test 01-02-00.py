# Databricks notebook source
# Lion: Test 01-02-A 
spark.sql('select count(*) as cnt \
from  hive_metastore.1_demo.player').show()

# COMMAND ----------

# Lion: Test 01-02-B
#

sc.setJobDescription("Lion: Test 01-02-B1")

spark.sql('select count(*) as cnt \
from  hive_metastore.1_demo.player').show()

sc.setJobDescription("Lion: Test 01-02-B2")
spark.sql('select LevelID, count(*) as cnt \
from  hive_metastore.1_demo.player a \
join  hive_metastore.1_demo.playerlevel b \
on  a.playerid = b.playerid \
join  hive_metastore.1_demo.activitytype c \
on  b.activitytypeid = c.activitytypeid \
where c.activitytypeid = 2   \
group by b.LevelID \
order by b.LevelID').show()

sc.setJobDescription("Lion: Test 01-02-B3")

spark.sql('select count(*) as cnt \
from  hive_metastore.1_demo.player').show()


# COMMAND ----------



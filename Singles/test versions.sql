-- Databricks notebook source

select count(*) from 1_demo.test4

-- COMMAND ----------

insert into  1_demo.test4
select * from 1_demo.playerlevel limit 10

-- COMMAND ----------

select *
from   1_demo.test4 limit 10

-- COMMAND ----------

update 1_demo.test4
set playerid = 1
where playerlevelid = 157983


-- COMMAND ----------

delete from 1_demo.test4
where playerlevelid = 157979

-- COMMAND ----------



-- COMMAND ----------

-- MAGIC %python
-- MAGIC display(dbutils.fs.ls("dbfs:/user/hive/warehouse/1_demo.db/test4"))

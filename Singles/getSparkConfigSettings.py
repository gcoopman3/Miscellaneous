# Databricks notebook source
xf = spark.sparkContext.getConf().getAll()
df = spark.createDataFrame(xf)
df.columns

df2 = df.withColumnRenamed("_1","ConfigName") \
        .withColumnRenamed("_2","ConfigValue")

df2.createOrReplaceTempView('vConfigurations')



# COMMAND ----------

# MAGIC 
# MAGIC %sql
# MAGIC 
# MAGIC select *
# MAGIC from vConfigurations-
# MAGIC --where Configname like "%Metastore%"

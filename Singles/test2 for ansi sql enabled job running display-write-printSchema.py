# Databricks notebook source
# display('This is a display test')

# COMMAND ----------

print('this is a write test')

# COMMAND ----------

from datetime import datetime, tzinfo, timedelta

class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)
      
v_utctime = datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()

# COMMAND ----------

sc.setJobDescription("test2 for ansi sql - sect 01 " + v_utctime)
df = spark.sql('select * from hive_metastore.1_demo.playerlevel limit 1')

# COMMAND ----------

sc.setJobDescription("test2 for ansi sql - sect 02 " + v_utctime)

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.sql.ansi.enabled = false

# COMMAND ----------

sc.setJobDescription("test2 for ansi sql - sect 03 " + v_utctime)
display(spark.sql("select   * from hive_metastore.1_demo.playerlevel limit 1"))

# COMMAND ----------

sc.setJobDescription("test2 for ansi sql - sect 04 " + v_utctime)

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.sql.ansi.enabled = true

# COMMAND ----------

sc.setJobDescription("test2 for ansi sql - sect 05 " + v_utctime)

# COMMAND ----------

# MAGIC %sql
# MAGIC select   cast('a' as int) test,* from hive_metastore.1_demo.playerlevel limit 1

# COMMAND ----------

#display(df)

# COMMAND ----------

df.printSchema()

# COMMAND ----------

print(df)

# COMMAND ----------

# MAGIC %sql
# MAGIC describe history hive_metastore.1_demo.playerlevel

# COMMAND ----------

# MAGIC %sh	
# MAGIC ps

# COMMAND ----------

# MAGIC %fs ls dbfs:/user/hive/warehouse/1_demo.db/playerlevel/_delta_log/

# COMMAND ----------

# MAGIC %sql
# MAGIC select *  from hive_metastore.1_demo.playerlevel limit 100

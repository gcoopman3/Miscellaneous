-- Databricks notebook source
-- MAGIC %md
-- MAGIC # Two Cells (Error occurs at the end of the second cell)
-- MAGIC   

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ###   Cell A: One Insert Command of one row (Always succeeds, even when error occurs in Cell 2)

-- COMMAND ----------

Insert into  table hive_metastore.default.mytable(id, name, state)
values (0, 'Nancy', 'Texas')

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## Cell B:  Three statements (none of the following Inserts succeed)
-- MAGIC       1. Insert two rows
-- MAGIC       2. Insert one row
-- MAGIC       3. Divide by Zero (will cause error)

-- COMMAND ----------

--No code in the cell will be committed because of 1/0 error at bottom of cell.

-- two inserts
Insert into  table hive_metastore.default.mytable(id, name, state)
values (1, 'Greg', 'Texas'),
(2, 'Robert', 'Florida');

--one insert
Insert into  table hive_metastore.default.mytable(id, name, state)
values (3, 'Ted', 'Oklahoma');

--following line will cause an error
Select 1/0 as ForceError

-- COMMAND ----------



-- COMMAND ----------

-- MAGIC %md
-- MAGIC # Work Flow Failure and "Repair Run" Option
-- MAGIC <BR></BR>
-- MAGIC ### Will a "Repair Run" button be available after a workflow fails?
-- MAGIC    1.  No, if there is only one task in the workflow (option will not be available)
-- MAGIC    2.  Yes, if there is more than one task in the workflow
-- MAGIC <BR></BR>
-- MAGIC ### What is the "Repair Run" button used for?
-- MAGIC   When an error occurs on a task in a workflow, causing it to fail, and if the workflow has more than one task, the 
-- MAGIC   "Repair Run" but will be visible and available so that you can re-run the job starting at the first line,
-- MAGIC   of the first cell of the notebook assigned to the respective task that failed.
-- MAGIC <BR></BR>
-- MAGIC ### ...Problem(s)?  Even if you can perform a "Repair Run"
-- MAGIC   If you have more than one task in the workflow and an error occurs failing the workflow, you can 
-- MAGIC   fix the error (in the above case, the Divide by zero error), you will then be able to activate "Repair Run". 
-- MAGIC   The run will begin at the task it errored on and it will begin execution at the the top of the notebook (cell A in the case above, \
-- MAGIC   WHICH HAD ALREADY BEEN COMPLETED AND COMMITTED). This means the row in the cell A will be run twice causing duplicate double entry, 
-- MAGIC   which is not the initial of the notebook.
-- MAGIC <BR></BR>
-- MAGIC ###  Recommendation:
-- MAGIC   1.  Always have more than one task in a workflow (that enusres that if error occurs, you can perfoem a "Repair Run").
-- MAGIC   2.  your task should only call notebooks that have only one cell of code (that way if the notebook fails you can fix the workflow
-- MAGIC       by performing a "Repair Run" without worry or unintentionally repeating previous code commits)
-- MAGIC   3.  If you have a very heavy notebook (lots of code where some code blocks have long processing durations), divide it into multiple notebooks, 
-- MAGIC       assign each "heavy" code block to a sperate task, and run new tasks in sequence. This way if an error occurs on any of the later tasks in the workflow, 
-- MAGIC       the workflow will not have to re-do the work of the ocmpleted long running code when running the "Repair Run" process.
-- MAGIC            
-- MAGIC   
-- MAGIC    

-- Databricks notebook source
-- MAGIC %md
-- MAGIC # Display Table Status between Iterations

-- COMMAND ----------

-- MAGIC %sql
-- MAGIC 
-- MAGIC delete from hive_metastore.default.mytable

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## Before First Run (First Iteration)

-- COMMAND ----------

-- MAGIC %scala
-- MAGIC val v_selectdf = (spark.sql("select * from hive_metastore.default.mytable order by id"))
-- MAGIC v_selectdf.show

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## Before First Run (First Iteration) - empty table

-- COMMAND ----------

-- MAGIC %scala
-- MAGIC val v_selectdf = (spark.sql("select * from hive_metastore.default.mytable order by id"))
-- MAGIC v_selectdf.show

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## After First Run (First Iteration) - Notebook attempts to insert 5 rows, but because of error only 4 are committed

-- COMMAND ----------

-- MAGIC %scala
-- MAGIC val v_selectdf = (spark.sql("select * from hive_metastore.default.mytable order by id"))
-- MAGIC v_selectdf.show

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## After correcting Divide by Zero Error  First Run (First Iteration) - Notebook attempts to insert 5 rows, but because of error only 4 are committed

-- COMMAND ----------

-- MAGIC %scala
-- MAGIC val v_selectdf = (spark.sql("select * from hive_metastore.default.mytable order by id"))
-- MAGIC v_selectdf.show

-- COMMAND ----------



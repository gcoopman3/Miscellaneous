-- Databricks notebook source
-- MAGIC %md
-- MAGIC # Work Flow Failure and "Repair Run" Option
-- MAGIC <BR></BR>
-- MAGIC ### Will a "Repair Run" button be available after a workflow fails?
-- MAGIC    1.  No, if there is only one task in the workflow (option will not be available)
-- MAGIC    2.  Yes, if there is more than one task in the workflow
-- MAGIC <BR></BR>
-- MAGIC ### What is the "Repair Run" button used for?
-- MAGIC   When an error occurs on a task in a workflow, causing it to fail, and if the workflow has more than one task, the  "Repair Run" button will be visible and enabled so that you can re-run the job starting at the first line,
-- MAGIC   of the first cell of the notebook assigned to the respective task that failed.
-- MAGIC <BR></BR>
-- MAGIC ### ...Problem(s)?  Even if you can perform a "Repair Run"
-- MAGIC   If you have more than one task in the workflow and an error occurs failing the workflow, you can fix the error (in the above case, the Divide by zero error), you will then be able to activate "Repair Run". 
-- MAGIC   The run will begin at the task it errored on and it will begin execution at the the top of the notebook (cell A in the case).  All core in the offending task, prior to the line of code that errored, will be committed. This means that if you run "Repair Run" all those line in the task will be run again, possibly causing duplicate rows and other data integrity issues.  Notebooks must be prepared for errors and self correcting when re-runned.
-- MAGIC <BR></BR>
-- MAGIC ###  Recommendation:
-- MAGIC   1.  Always have more than one task in a workflow (that ensures that if an error occurs, you can perform a "Repair Run").
-- MAGIC   2.  Proactively plan for failure, and what operator needs to do in case of failure (SOP) to ensure data is not logically corrupted (like duplicates, etc)  "If logic" or some other mechanism to save "State" may need to be added before every action that effects the data to ensure data integrity if a "Repair Run" needs to be performed.
-- MAGIC   3.  If you have a very heavy notebook (lots of code where some code blocks have long processing durations), divide it into multiple notebooks, 
-- MAGIC       assign each "heavy" code block to a seperate task, and run new tasks in sequence. This way if an error occurs on any of the later tasks in the workflow, 
-- MAGIC       the workflow will not re-do the same code that will not only take a lot more time, but may cause data integrity issues.
-- MAGIC            
-- MAGIC   
-- MAGIC    

-- COMMAND ----------

-- MAGIC %md
-- MAGIC # Two Cell Notebook (Error occurs at the near end of the cell B below)
-- MAGIC   

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ###   Cell A: One Insert Command of one row (Always succeeds, even when error occurs in Cell 2)

-- COMMAND ----------

Insert into  table hive_metastore.default.mytable(id, name, state)
values (0, 'Nancy', 'Texas')

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## Cell B:  Two statements succeed, but third one after divide by zero does not
-- MAGIC       1. Insert two rows
-- MAGIC       2. Insert one row
-- MAGIC       3. Divide by Zero (will cause error)

-- COMMAND ----------

--No code in the cell will be committed because of 1/0 error at bottom of cell.

-- two inserts
Insert into  table hive_metastore.default.mytable(id, name, state)
values (1, 'Greg', 'Texas'),
(2, 'Robert', 'Florida');

--one insert
Insert into  table hive_metastore.default.mytable(id, name, state)
values (3, 'Ted', 'Oklahoma');

--following line will cause an error
--Select 1/0 as ForceError


--one insert
Insert into  table hive_metastore.default.mytable(id, name, state)
values (4, 'jesse', 'Texas');

-- COMMAND ----------



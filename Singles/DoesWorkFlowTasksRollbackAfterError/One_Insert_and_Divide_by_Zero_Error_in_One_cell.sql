-- Databricks notebook source
--Insert will fail because of error in same cell, if insert was in a separate cell before the divide by zero, then the insert would be committed even when it is in the same notebook
--You can change this code or comment it all out, then go to Job 
Insert into  table hive_metastore.default.mytable(id, name, state)
values (4, 'Sally', 'Nevada')

Select 1/0 as ForceError

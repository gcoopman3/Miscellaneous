# Databricks notebook source

def deep_ls(path: str):
    """List all files in base path recursively."""
    for x in dbutils.fs.ls(path):
        if x.path[-1] != '/':
            yield x
        else:
            for y in deep_ls(x.path):
                yield y
                
print(deep_ls("dbfs:/user/hive/warehouse/1_demo.db/activitytype"))                

# COMMAND ----------

def get_dir_content(ls_path):
  dir_paths = dbutils.fs.ls(ls_path)
  subdir_paths = [get_dir_content(p.path) for p in dir_paths if p.isDir() and p.path != ls_path]
  flat_subdir_paths = [p for subdir in subdir_paths for p in subdir]
  return list(map(lambda p: p.path, dir_paths)) + flat_subdir_paths

paths = get_dir_content('dbfs:/')

# COMMAND ----------

def get_dir_content(ls_path):
  dir_paths = dbutils.fs.ls(ls_path)
  subdir_paths = [get_dir_content(p.path) for p in dir_paths if p.isDir() and p.path != ls_path]
  flat_subdir_paths = [p for subdir in subdir_paths for p in subdir]
  return list(map(lambda p: p.path, dir_paths)) + flat_subdir_paths

paths = get_dir_content('dbfs:/user/hive/warehouse/1_demo.db/activitytype')
print(paths)

# COMMAND ----------

display(dbutils.fs.ls("dbfs:/user/hive/warehouse/1_demo.db/test4"))


# COMMAND ----------

#display files

df = dbutils.fs.ls("dbfs:/user/hive/warehouse/1_demo.db/playerlevel/_delta_log") 
display(df.count)

# COMMAND ----------

# get number of files in a path 

paths = get_dir_content('dbfs:/user/hive/warehouse/1_demo.db/playerlevel')

print(len([p for p in paths]))

# COMMAND ----------

# get number of files in a path 

paths = get_dir_content('dbfs:/user/hive/warehouse/1_demo.db/player')

print(len([p for p in paths]))

# COMMAND ----------

# get number of files in a path 

paths = get_dir_content('dbfs:/user/hive/warehouse/1_demo.db')

print(len([p for p in paths]))

# COMMAND ----------

# MAGIC %sql
# MAGIC SHOW TBLPROPERTIES hive_metastore.1_demo.playerlevel; 

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC alter table hive_metastore.1_demo.playerlevel
# MAGIC ADD PARTITION activityetypeid

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC select * from hive_metastore.1_demo.playerlevel limit 100

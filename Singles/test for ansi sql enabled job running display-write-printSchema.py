# Databricks notebook source
# display('This is a display test')

# COMMAND ----------

print('this is a write test')

# COMMAND ----------

from datetime import datetime, tzinfo, timedelta

class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)
      
v_utctime = datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()

# COMMAND ----------

sc.setJobDescription("test1 for ansi sql - sect 01a " + v_utctime)
df = spark.sql('select levelid, count(*) from hive_metastore.1_demo.playerlevel group by levelid order by 1')
#display(df)
df.write.format('noop').mode('overwrite').save()

# COMMAND ----------

sc.setJobDescription("test1 for ansi sql - sect 02 " + v_utctime)
df = spark.sql('select levelid, count(*) from hive_metastore.1_demo.playerlevel group by levelid order by count(*)')
#display(df)
df.write.format('noop').mode('overwrite').save()

# COMMAND ----------

df = spark.sql("set spark.sql.ansi.enabled = false")
#print(df)

# COMMAND ----------

sc.setJobDescription("test1 for ansi sql - sect 03 " + v_utctime)
df = spark.sql("select   * from hive_metastore.1_demo.playerlevel order by 1")
df.write.format('noop').mode('overwrite').save()

# COMMAND ----------



# COMMAND ----------

#print("set spark.sql.ansi.enabled = true")

# COMMAND ----------

sc.setJobDescription("test for ansi sql - sect 05 " + v_utctime)
df = spark.sql("select   cast('a' as int) test,* from hive_metastore.1_demo.playerlevel limit 1")

# COMMAND ----------

#display(df)

# COMMAND ----------

#df.printSchema()

# COMMAND ----------

#print(df)

# COMMAND ----------

#describe history hive_metastore.1_demo.playerlevel

# COMMAND ----------

# MAGIC %sh	
# MAGIC ps

# COMMAND ----------

# MAGIC %fs ls dbfs:/user/hive/warehouse/1_demo.db/playerlevel/_delta_log/

# COMMAND ----------

#%sql
#select *  from hive_metastore.1_demo.playerlevel limit 100

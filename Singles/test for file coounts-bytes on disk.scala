// Databricks notebook source
// MAGIC %scala
// MAGIC 
// MAGIC //number of files from azure storage
// MAGIC import com.databricks.sql.transaction.tahoe.DeltaLog
// MAGIC import org.apache.hadoop.fs.Path
// MAGIC  
// MAGIC val deltaPath = "dbfs:/user/hive/warehouse/1_demo.db/playerlevel"
// MAGIC val deltaLog = DeltaLog.forTable(spark, deltaPath)
// MAGIC val currentFiles = deltaLog.snapshot.allFiles
// MAGIC display(currentFiles.groupBy("partitionValues.col").count().orderBy($"count".desc))

// COMMAND ----------

// MAGIC %scala
// MAGIC 
// MAGIC //number of files from azure storage
// MAGIC import com.databricks.sql.transaction.tahoe.DeltaLog
// MAGIC import org.apache.hadoop.fs.Path
// MAGIC  
// MAGIC val deltaPath = "dbfs:/user/hive/warehouse/1_demo"
// MAGIC val deltaLog = DeltaLog.forTable(spark, deltaPath)
// MAGIC val currentFiles = deltaLog.snapshot.allFiles
// MAGIC display(currentFiles.groupBy("partitionValues.col").count().orderBy($"count".desc))

// COMMAND ----------

// MAGIC %fs ls dbfs:/user/hive/warehouse/1_demo.db/playerlevel

// COMMAND ----------

// MAGIC 
// MAGIC %scala
// MAGIC //file size on disk
// MAGIC 
// MAGIC import com.databricks.sql.transaction.tahoe._
// MAGIC //val deltaPath = "abfs://deposit-new@psharshastorage25000.dfs.core.windows.net/bronze/cpudynamicdata_cpudynamicdata"
// MAGIC val deltaPath = "dbfs:/user/hive/warehouse/1_demo.db/playerlevel"
// MAGIC 
// MAGIC val deltaLog = DeltaLog.forTable(spark, deltaPath)
// MAGIC //val snapshot = deltaLog.unsafeVolatileSnapshot               // the current delta table snapshot
// MAGIC //println(deltaLog.unsafeVolatileSnapshot.sizeInBytes * 0.000001)
// MAGIC println(s"Total file size (Bytes): ${deltaLog.snapshot.sizeInBytes}")
// MAGIC println(s"Total file size (MB): ${deltaLog.snapshot.sizeInBytes * 0.000001}")
// MAGIC //1,000,000,000
// MAGIC println(s"Total file size (GB): ${deltaLog.snapshot.sizeInBytes /1000000000}")

// COMMAND ----------

// MAGIC %sql
// MAGIC describe extended hive_metastore.1_demo.playerlevel

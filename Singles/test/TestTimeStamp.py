# Databricks notebook source
# MAGIC %sql
# MAGIC select current_timestamp()

# COMMAND ----------

display(spark.sql.session.timeZone.)

# COMMAND ----------

# MAGIC %sql
# MAGIC select current_timezone()

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Set time zone to the system default.
# MAGIC SET TIME ZONE LOCAL;
# MAGIC 
# MAGIC -- Set time zone to the region-based zone ID.
# MAGIC SET TIME ZONE 'America/Los_Angeles';
# MAGIC 
# MAGIC --SET TIME ZONE 'America/Chicago';

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Set time zone to the Zone offset. 
# MAGIC SET TIME ZONE '+08:00';
# MAGIC 
# MAGIC -- Set time zone with intervals.
# MAGIC SET TIME ZONE INTERVAL 1 HOUR 30 MINUTES;
# MAGIC SET TIME ZONE INTERVAL '08:30:00' HOUR TO SECOND;

# COMMAND ----------

# MAGIC %sql
# MAGIC select current_timezone()

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Set time zone to the region-based zone ID.
# MAGIC SET TIME ZONE 'Etc/UTC';
# MAGIC 
# MAGIC select current_timezone();

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Set time zone to the system default.
# MAGIC SET TIME ZONE LOCAL;
# MAGIC 
# MAGIC 
# MAGIC select current_timezone()

# COMMAND ----------

# MAGIC %sql
# MAGIC select cast('2022-01-11 16:00:00' as timestamp)

# COMMAND ----------

# MAGIC %sql
# MAGIC create temp table #myTempTable as (myDate datetime);
# MAGIC insert into #myTempTable ('2022-12-25 08:00:00');
# MAGIC 
# MAGIC 
# MAGIC select * --cast(select cast(cast(MyDate as varchar(100)) as timestamp)
# MAGIC from #myTempTable

# COMMAND ----------

# MAGIC %sql
# MAGIC create table myTable2 (someDate timestamp)

# COMMAND ----------

# MAGIC %sql
# MAGIC insert into myTable2(someDate)
# MAGIC values('2022-12-25 8:00:00')

# COMMAND ----------

# MAGIC %sql
# MAGIC select someDate, cast(someDate as TimeStamp) someDate2, cast('2022-12-25 8:00:00' as TimeStamp) someDate3 from myTable2

# COMMAND ----------

# MAGIC 
# MAGIC %sql
# MAGIC select getdate() somedate5  --current CST 5:24 pm

# COMMAND ----------

# MAGIC %sql
# MAGIC select cast(getdate() as TimeStamp) somedate4  --current CST 5:24 pm

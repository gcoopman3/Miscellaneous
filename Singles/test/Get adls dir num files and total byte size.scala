// Databricks notebook source
val mounts = dbutils.fs.mounts()

val dfm = mounts.toDF()

display(dfm)

// COMMAND ----------



val path="/mnt/datalakehousepoc/trainmaterial"

val filelist=dbutils.fs.ls(path)


val df = filelist.toDF() 
df.createOrReplaceTempView("adlsSize")

spark.sql("select *, count(*) over(partition by 1) NumberOfFiles, MAX(SIZE) over(partition by 1) max , sum(size) over () s, sum(size) over (partition by 1)/(1024*1024*1024) as sizeInGB from adlsSize").show()


// COMMAND ----------

display(spark.sql("select sum(size), 360/(1024*1024*1024) as big from adlsSize"))


// COMMAND ----------



val path="/mnt/datalakehousepoc/trainmaterial"

val filelist=dbutils.fs.ls(path)

val df = filelist.toDF() 
df.createOrReplaceTempView("adlsSize")


display(spark.sql("select count(*) NumberOfFiles, sum(size)/(1024*1024*1024) as sizeInGB from adlsSize"))

// COMMAND ----------

spark.sql("select * from adlsSize").show()



// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC import pandas as pd
// MAGIC 
// MAGIC pandasDF = df.toPandas()
// MAGIC print(pandasDF)
// MAGIC 
// MAGIC pandasDF.summary().show()

// COMMAND ----------

// MAGIC %python
// MAGIC !! pip install com.databricks.sql.transaction.tahoe

// COMMAND ----------

	import com.databricks.sql.transaction.tahoe.DeltaLog
	import org.apache.hadoop.fs.Path


val deltaPath = "<table_path>"
val deltaLog = DeltaLog(spark, new Path(deltaPath + "/_delta_log"))
val currentFiles = deltaLog.snapshot.allFiles
display(currentFiles.groupBy("partitionValues.col").count().orderBy($"count".desc))

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC describe extended hive_metastore.1_demo.playerlevel

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC from pyspark.sql import DataFrame
// MAGIC from pyspark.sql import functions as f
// MAGIC 
// MAGIC 
// MAGIC path = "dbfs:/user/hive/warehouse/1_demo.db/playerlevel"
// MAGIC partition_cols = spark.sql(f'describe detail delta.`{path}`').select('partitionColumns').collect()[0][0]
// MAGIC 
// MAGIC JDeltaLog = spark._jvm.org.apache.spark.sql.delta.DeltaLog
// MAGIC all_files_jdf = JDeltaLog.forTable(spark._jsparkSession, path).snapshot().allFiles().toDF()
// MAGIC all_files_df = DataFrame(all_files_jdf, spark._wrapped)
// MAGIC partition_counts_df = (
// MAGIC     all_files_df
// MAGIC     .groupBy([f.col('partitionValues')[key].alias(key) for key in partition_cols])
// MAGIC     .count()
// MAGIC )

// COMMAND ----------

import com.databricks.sql.transaction.tahoe._
val deltaLog = DeltaLog.forTable(spark, "dbfs:/user/hive/warehouse/1_demo.db/playerlevel")
val snapshot = deltaLog.snapshot               // the current delta table snapshot


println(s"Total file size (bytes): ${deltaLog.snapshot.sizeInBytes}")


// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC import pandas as pd                               # Load pandas
// MAGIC 
// MAGIC data = pd.DataFrame({'x1':[2, 7, 5, 7, 1, 5, 9],  # Create pandas DataFrame
// MAGIC                      'x2':range(1, 8),
// MAGIC                      'group':['A', 'B', 'A', 'A', 'C', 'B', 'A']})
// MAGIC print(data)                                       # Print pandas DataFrame
// MAGIC 
// MAGIC print(data['x1'].mean())                          # Get mean of one column
// MAGIC # 5.142857142857143
// MAGIC 
// MAGIC print(data.mean(numeric_only = True))             # Get mean of all columns
// MAGIC # x1    5.142857
// MAGIC # x2    4.000000
// MAGIC # dtype: float64
// MAGIC 
// MAGIC print(data.describe())                            # Get descriptive statistics of all column
// MAGIC #              x1        x2
// MAGIC # count  7.000000  7.000000
// MAGIC # mean   5.142857  4.000000
// MAGIC # std    2.853569  2.160247
// MAGIC # min    1.000000  1.000000
// MAGIC # 25%    3.500000  2.500000
// MAGIC # 50%    5.000000  4.000000
// MAGIC # 75%    7.000000  5.500000
// MAGIC # max    9.000000  7.000000
// MAGIC 
// MAGIC print(data.groupby('group').mean())               # Get mean by group
// MAGIC #          x1    x2
// MAGIC # group            
// MAGIC # A      5.75  3.75
// MAGIC # B      6.00  4.00
// MAGIC # C      1.00  5.00

// COMMAND ----------

CREATE TABLE T(c1 INT) TBLPROPERTIES('this.is.my.key' = 12, this.is.my.key2 = true); 
SHOW TBLPROPERTIES T; 

// COMMAND ----------

// MAGIC %sql
// MAGIC select * from hive_metastore.1_demo.playerlevel limit 10

// COMMAND ----------

// MAGIC %sql
// MAGIC SHOW TBLPROPERTIES hive_metastore.1_demo.playerlevel

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC alter table hive_metastore.1_demo.playerlevel add  partition (ActivityTypeID2 integer)
// MAGIC 
// MAGIC --alter table test_opencsv_part add partition(dt = 'bar') location 'oss://oss-cn-beijing-for-openanalytics-test/datasets/test/test_opencsv_part/dt=foo/';

// COMMAND ----------

// MAGIC %sql 
// MAGIC show partitions hive_metastore.1_demo.playerlevel

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC CREATE TABLE hive_metastore.1_demo.test (id integer, name string, state string)

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC CREATE TABLE hive_metastore.1_demo.test2 (id integer, name string) partitioned by (state string)

// COMMAND ----------

// MAGIC %sql
// MAGIC describe table hive_metastore.1_demo.test2

// COMMAND ----------

// MAGIC %sql
// MAGIC SHOW TBLPROPERTIES hive_metastore.1_demo.test2

// COMMAND ----------

// MAGIC %sql
// MAGIC insert into hive_metastore.1_demo.test2 (id, name, state) select 1, 'rob', 'ga'

// COMMAND ----------

// MAGIC %sql
// MAGIC show partitions hive_metastore.1_demo.test2

// COMMAND ----------



// COMMAND ----------

// MAGIC %sql
// MAGIC OPTIMIZE hive_metastore.1_demo.test2
// MAGIC zorder by (name)

// COMMAND ----------

// MAGIC %sql 
// MAGIC DESCRIBE HISTORY  hive_metastore.1_demo.test2

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC df= spark.sql('describe history hive_metastore.1_demo.test2')
// MAGIC df.createOrReplaceTempView("vw_FindZOrder")
// MAGIC df2 = spark.sql("select * from vw_FindZOrder  where  operationParameters.zOrderBy != '' order by timestamp desc ")
// MAGIC display(df2)

// COMMAND ----------

// find if table has a bloom filter index


spark.table("hive_metastore.1_demo.test2").schema.foreach(field => println(s"${field.name}: metadata=${field.metadata}"))

// COMMAND ----------

// MAGIC %sql
// MAGIC select * from hive_metastore.1_demo.test2

// COMMAND ----------

// MAGIC %sql
// MAGIC CREATE BLOOMFILTER INDEX
// MAGIC ON TABLE hive_metastore.1_demo.test2
// MAGIC FOR COLUMNS(name OPTIONS (fpp=0.1, numItems=50000000))

// COMMAND ----------

// MAGIC %sql
// MAGIC SET spark.databricks.delta.optimize.maxFileSize = 1600000000;
// MAGIC OPTIMIZE hive_metastore.1_demo.test2
// MAGIC ZORDER BY name

// COMMAND ----------

// find if table has a bloom filter index


spark.table("hive_metastore.1_demo.test2").schema.foreach(field => println(s"${field.name}: metadata=${field.metadata}"))

// COMMAND ----------

import spark.implicits._
import org.apache.spark.sql._

val df = spark.sql("show databases")

case class cls_Database(databaseName:String)
  df.as[cls_Database].take(df.count.toInt).foreach(t => println(s"${t.databaseName}, (spark.sql('show tables')"))) 
//df.as[cls_Database].take(df.count.toInt).foreach(t => s"println(spark.sql('show tables'))")                                         
//df.as[cls_Database].take(df.count.toInt).foreach(t => println(s"${t.databaseName}"))



// COMMAND ----------

// MAGIC %python
// MAGIC spark.sql('show databases').show()

// COMMAND ----------

// MAGIC %python
// MAGIC #spark.sql('use dbacademy_greg_coopman_sqlcoop_com_adewd_5_1')
// MAGIC spark.sql('use 1_demo')
// MAGIC display(spark.sql('show tables from 1_demo'))

// COMMAND ----------

// MAGIC %python
// MAGIC display(spark.catalog.listDatabases())

// COMMAND ----------

// MAGIC %python
// MAGIC import pandas as pd
// MAGIC 
// MAGIC Database_list = spark.catalog.listDatabases()
// MAGIC display(pd.DataFrame(spark.catalog.listDatabases()))
// MAGIC #display(pd.DataFrame(spark.catalog.listTables()))
// MAGIC 
// MAGIC display(pd.DataFrame(spark.catalog.listTables()))

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC spark.catalog.listColumns(tableName=tbl.name, dbName=db.name)

// COMMAND ----------

// MAGIC %python
// MAGIC import pandas as pd
// MAGIC 
// MAGIC #display(pd.DataFrame(spark.catalog.listDatabases()))
// MAGIC #display(pd.DataFrame(spark.catalog.listTables()))
// MAGIC 
// MAGIC Database_list = spark.catalog.listDatabases()
// MAGIC 
// MAGIC 
// MAGIC for db in Database_list:
// MAGIC   print('database: ',db.name)
// MAGIC   Table_list = spark.catalog.listTables(dbName='{0}'.format(db.name))
// MAGIC   for tbl in Table_list:
// MAGIC         print('   table: ',tbl.name)
// MAGIC         fullTableName = (db.name + '.' +tbl.name)
// MAGIC         Column_list = spark.catalog.listColumns(tableName='{0}'.format(fullTableName))
// MAGIC         for col in Column_list:
// MAGIC           print('       col: ', col.name)
// MAGIC           

// COMMAND ----------

// MAGIC %python
// MAGIC Out[8]

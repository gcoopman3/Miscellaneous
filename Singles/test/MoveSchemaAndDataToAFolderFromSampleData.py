# Databricks notebook source
json_content1 = "{'json_col1': 'hello', 'json_col2': 32}"
json_content2 = "{'json_col1': 'hello', 'json_col2': 'world'}"

json_list = []
json_list.append(json_content1)
json_list.append(json_content2)

df = spark.read.json(sc.parallelize(json_list))
display(df)

# COMMAND ----------

str_dict = str(df.schema.jsonValue())

# COMMAND ----------

dbutils.fs.mounts()

# COMMAND ----------

dbutils.fs.put("/mnt/datalakehousepoc/trainmaterial/SchemaGregReceive.dict", str_dict, True)

# COMMAND ----------

dbutils.fs.mounts()


# COMMAND ----------

df.coalesce(1).write.json('/mnt/datalakehousepoc/trainmaterial/GregReceive_v2.json')

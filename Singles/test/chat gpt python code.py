# Databricks notebook source
from pyspark.sql import SparkSession
import pandas as pd

# create a Spark session
spark = SparkSession.builder.appName("Read Hive Table").getOrCreate()

# read the Hive table into a Spark DataFrame
df = spark.sql("SELECT * FROM 1_demo.player")

# convert the Spark DataFrame to a Pandas DataFrame
pandas_df = df.toPandas()

# show the Pandas DataFrame
#print(pandas_df.head())
print(pandas_df.describe())
print(pandas_df.head())
#df2 = pandas_df.describe()
#display(df2)


# COMMAND ----------

import pandas as pd

# create a sample dataframe
data = {'col1': [1, 2, 3, 4], 'col2': [10, 20, 30, 40]}
# df = pd.DataFrame(data)
df = pd.DataFrame('1_demo.playerlevel')
# generate summary statistics
print(df.describe())


# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC describe 1_demo.playerlevel;

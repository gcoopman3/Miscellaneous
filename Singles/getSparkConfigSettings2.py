# Databricks notebook source

xf = spark.sparkContext.getConf().getAll()
df = spark.createDataFrame(xf)
df.columns

# COMMAND ----------

df2 = df.withColumnRenamed("_1","ConfigName") \
        .withColumnRenamed("_2","ConfigValue")

display(df2)


# COMMAND ----------



# COMMAND ----------

df2.filter(df2["ConfigName"] == "spark.driver.tempDirectory").show()
#df.filter(df("state") === "OH").show()


# COMMAND ----------

df2.createOrReplaceTempView('vConfigurations')

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC select *
# MAGIC from vConfigurations
# MAGIC where Configname like "%version%"

# COMMAND ----------

spark.conf.get("spark.sql.shuffle.partitions")



# COMMAND ----------

# MAGIC %scala
# MAGIC 
# MAGIC spark.conf.set("spark.sql.adaptive.enabled",true)
# MAGIC spark.conf.set("spark.sql.adaptive.coalescePartitions.enabled",false)
# MAGIC spark.conf.set("spark.sql.adaptive.skewJoin.enabled",false)
# MAGIC 
# MAGIC spark.conf.get("")

# COMMAND ----------

print(spark.conf.get("spark.sql.adaptive.enabled"))
print(spark.conf.get("spark.sql.adaptive.coalescePartitions.enabled"))
print(spark.conf.get("spark.sql.adaptive.skewJoin.enabled"))



# COMMAND ----------

# MAGIC %scala
# MAGIC 
# MAGIC spark.conf.set("spark.sql.adaptive.enabled",true)
# MAGIC spark.conf.set("spark.sql.adaptive.coalescePartitions.enabled",true)
# MAGIC spark.conf.set("spark.sql.adaptive.skewJoin.enabled", true)

# COMMAND ----------

print(spark.conf.get("spark.sql.adaptive.enabled"))
print(spark.conf.get("spark.sql.adaptive.coalescePartitions.enabled"))
print(spark.conf.get("spark.sql.adaptive.skewJoin.enabled"))


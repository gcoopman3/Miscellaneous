# Databricks notebook source
# MAGIC %sql
# MAGIC create or replace temporary view NullCompA as
# MAGIC select 1 as id, "Greg" as name
# MAGIC union
# MAGIC select 2 as id, "Robert" as name
# MAGIC union
# MAGIC select 3 as id, NULL as name

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from NullCompA

# COMMAND ----------

# MAGIC %sql
# MAGIC create or replace temporary view NullCompB as
# MAGIC select 1 as id, NULL as name, "Texas" as state
# MAGIC union
# MAGIC select 2 as id, "Robert" as name, "Florida" as state
# MAGIC union
# MAGIC select 3 as id, NULL as name, "Georgia" as state

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from NullCompB

# COMMAND ----------

# MAGIC %sql
# MAGIC select a.id, a.name, b.state
# MAGIC from NullCompA a
# MAGIC join NullCompB b
# MAGIC on   a.id <=> b.id and a.name = b.name

# COMMAND ----------

# MAGIC %sql
# MAGIC select a.id, a.name, b.state
# MAGIC from NullCompA a
# MAGIC join NullCompB b
# MAGIC on   a.id = b.id and a.name <=> b.name

# COMMAND ----------

# MAGIC %sql
# MAGIC select a.id, a.name, b.state
# MAGIC from NullCompA a
# MAGIC join NullCompB b
# MAGIC on   a.id <=> b.id and a.name <=> b.name

# COMMAND ----------

# MAGIC %sql
# MAGIC select a.id, a.name, b.state
# MAGIC from NullCompA a
# MAGIC join NullCompB b
# MAGIC on   a.id = b.id and a.name = b.name

# COMMAND ----------



-- Databricks notebook source
>
SELECT
  from_utc_timestamp('2016-08-31', 'Asia/Seoul');
2016 -08 -31 09 :00 :00 >
SELECT
  from_utc_timestamp('2017-07-14 02:40:00.0', 'GMT+1');
'2017-07-14 03:40:00.0'

-- COMMAND ----------

from_unixtime((e.Job_startTime / 1000)) workflow_run_start_time

-- COMMAND ----------

on a.date > b.max_date - 7

# Databricks notebook source
# MAGIC %fs ls 

# COMMAND ----------

# MAGIC %md
# MAGIC # Place import file in dbfs:/FileStore
# MAGIC ### Load into python dataframe

# COMMAND ----------

# MAGIC %python
# MAGIC #need absolute path below
# MAGIC df= spark.read.format("csv").option("header",True).load("dbfs:/FileStore/cost_analysis.csv")

# COMMAND ----------

display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC #From Dataframe load into TempView
# MAGIC ###then run SQL statement against view from SQL

# COMMAND ----------

df.createOrReplaceTempView("VMCostsDaily")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from VMCostsDaily
# MAGIC where UsageDate = '2023-01-17'

# Databricks notebook source
# MAGIC %md
# MAGIC #### File System

# COMMAND ----------

	dbutils.fs.mounts()

# COMMAND ----------

storage_account_name = "datalakehousepoc"
storage_account_access_key = "28l6cQgK8DvZO/kbecmYHUmiVWBA4AdzhNGv1PEoE3Js5LGDA7y//FppXIbtYnOpNEjGzJvGHiw876Fg/TLJaw=="

# COMMAND ----------

spark.conf.set(
  "fs.azure.account.key."+storage_account_name+".dfs.core.windows.net",
  storage_account_access_key)


# COMMAND ----------

display(dbutils.fs.ls("abfss://trainmaterial@datalakehousepoc.dfs.core.windows.net"))

# COMMAND ----------

#gctest4io azure-webjobs-hosts
storage_account_name = "gctest4io"
storage_account_access_key = "lCkw/yrI4msXoL1qa2/XriAwW3EF7dBXguiu0so+EykSZQdq8nx3qKZGhLZ4+ZxXGRQjmrTQR27xPiXRBkqcAw=="

spark.conf.set(
  "fs.azure.account.key."+storage_account_name+".dfs.core.windows.net",
  storage_account_access_key)

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC create schema gctest4io location 'abfss://gctest4io/azure-webjobs-hosts'
# MAGIC --USE CATALOG <catalog>;
# MAGIC --CREATE { DATABASE | SCHEMA } [ IF NOT EXISTS ] <schema_name>
# MAGIC --    [ MANAGED LOCATION '<location_path>' ]
# MAGIC --    [ COMMENT <comment> ]
# MAGIC --    [ WITH DBPROPERTIES ( <property_key = property_value [ , ... ]> ) ];

# COMMAND ----------

# MAGIC %sql
# MAGIC --CREATE SCHEMA customer_sc MANAGED LOCATION 'abfss://us-east-1/finance'
# MAGIC create schema _train 

# COMMAND ----------

# MAGIC %sql
# MAGIC --CREATE TABLE <catalog>.<schema>.<table_name>
# MAGIC --(
# MAGIC --  <column_specification>
# MAGIC --)
# MAGIC --LOCATION 'abfss://<bucket_path>/<table_directory>';
# MAGIC 
# MAGIC create table hive_metastore._train.player
# MAGIC (id integer)
# MAGIC location 'abfss://trainmaterial@datalakehousepoc.dfs.core.windows.net/player'

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TABLE _train.playerlevel
# MAGIC using 1_demo.playerlevel

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from _train.player

# COMMAND ----------

# MAGIC %sql 
# MAGIC --COPY INTO _train.playerlevel
# MAGIC select * FROM "dbfs:/user/hive/warehouse/1_demo.db/playerlevel"

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from "abfss://trainmaterial@datalakehousepoc.dfs.core.windows.net/"

# COMMAND ----------

#display(dbutils.fs.ls("/mnt/datalakehousecbronze2"))

display(dbutils.fs.ls("/mnt/datalakehousepoc/trainmaterial"))

# COMMAND ----------

dbutils.fs.help()



# COMMAND ----------

dbutils.fs.mkdirs("/testdir1")

# COMMAND ----------

display(dbutils.fs.ls("/mnt/datalakehousepoc/trainmaterial"))



# COMMAND ----------

dbutils.fs.help()

# COMMAND ----------

file_list = dbutils.fs.ls("dbfs:/user/hive/warehouse/1_demo.db")
df = spark.createDataFrame(file_list)
type(df)
display(df)

# COMMAND ----------

file_list = dbutils.fs.ls("dbfs:/user/hive/warehouse/1_demo.db/playerlevel")
df = spark.createDataFrame(file_list)
type(df)
display(df)

# COMMAND ----------

# MAGIC %sql
# MAGIC describe detail 1_demo.playerlevel

# COMMAND ----------


display(spark.sql("select * from 1_demo.playerlevel limit 1"))

# COMMAND ----------

# MAGIC %md
# MAGIC # Start Here after loading function

# COMMAND ----------

file_list = get_dir_content('dbfs:/user/hive/warehouse/1_demo.db')
print(4)
[print(p) for p in file_list]
#[print(p) for p in flat_subdir_paths]
#type(file_output)
type(file_list)
#(file_list)

#df = spark.createDataFrame(list)
#df.createOrReplaceTempView("file_output")


# COMMAND ----------

# MAGIC %md
# MAGIC #Get Dir Content function

# COMMAND ----------

def get_dir_content(ls_path):
  print(0)
  dir_paths = dbutils.fs.ls(ls_path)
  print(1)
  subdir_paths = [get_dir_content(p.path) for p in dir_paths if p.isDir() and p.path != ls_path]
  print(2)
  flat_subdir_paths = [p for subdir in subdir_paths for p in subdir]
  print(3)
  return list(map(lambda p: p.path, dir_paths)) + flat_subdir_paths


# COMMAND ----------

#get database location
display(spark.sql("describe schema 1_demo"))

# COMMAND ----------

dfpL = spark.sql("select * from 1_demo.playerlevel")
dfpL.summary().show()

# COMMAND ----------



# COMMAND ----------

#get total file of a folder
folder_path = "dbfs:/user/hive/warehouse/1_demo.db"
file_list = dbutils.fs.ls(f"{folder_path}")

folder_path = "dbfs:/user/hive/warehouse/1_demo.db/playerlevel"
file_list = dbutils.fs.ls(f"{folder_path}")




type(file_list)
df = spark.createDataFrame(file_list)

df.createOrReplaceTempView("file_output")


# COMMAND ----------

#folder_path = "abfss://eim-datalake-prod@scadlsg2datbksprod.dfs.core.windows.net/datalake/db/ods_db_cdc"
#folder_path = "abfss://aasbackups@aasbackup20190101.dfs.core.windows.net/test2"
#file_list = dbutils.fs.ls(f"{folder_path}")

display(dbutils.fs.ls('abfss://aasbackups@aasbackup20190101.dfs.core.windows.net/test2'))


# COMMAND ----------

# MAGIC %sql
# MAGIC select * from file_output

# COMMAND ----------

display(spark.sql("describe detail hive_metastore.1_demo.playerlevel"))

# COMMAND ----------

abfss://fort-lauderdale@tenniscenterdailydropbox.dfs.core.windows.net/external-location/path/to/data"
/tenniscenterdailydropbox.dfs.core.windows.net/

# COMMAND ----------

dbutils.fs.mkdirs("/testdir1/testsubdir1")


# COMMAND ----------

display(dbutils.fs.ls("/testdir1"))


# COMMAND ----------

display(dbutils.fs.ls("/cluster-logs/"))


# COMMAND ----------

dbutils.fs.put("greg.txt","This is a text file example")

# COMMAND ----------

dbutils.fs.head("greg.txt")

# COMMAND ----------

dbutils.fs.mv("greg.txt","/testdir1")

# COMMAND ----------

dbutils.fs.cp("/testdir1/greg.txt","/")

# COMMAND ----------

display(dbutils.fs.ls("/"))

# COMMAND ----------

display(dbutils.fs.ls("/testdir1"))

# COMMAND ----------

dbutils.fs.rm("/greg.txt")

# COMMAND ----------

dbutils.library.installPyPI("numpy", version="1.15.4")
dbutils.library.restartPython()

# COMMAND ----------

dbutils.library.list()

# COMMAND ----------

# MAGIC %md
# MAGIC # Notebook Widgets

# COMMAND ----------

dbutils.widgets.text("api_key", "", "API Key")

# COMMAND ----------

dbutils.widgets.get("api_key")

# COMMAND ----------

dbutils.widgets.removeAll()

# COMMAND ----------

# MAGIC %md
# MAGIC #### NotebookWorkflows
# MAGIC 
# MAGIC "%md  #### Notebook Workflows"

# COMMAND ----------

dbutils.notebook.run("Workflow Sample", timeout_seconds=5)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Library

# COMMAND ----------

dbutils.library.installPyPI("sympy")

# COMMAND ----------

# MAGIC %md
# MAGIC ### Secrets

# COMMAND ----------

dbutils.secrets.listScopes()

# COMMAND ----------

dbutils.secrets.list("Azure")

# COMMAND ----------

dbutils.secrets.get("Azure", "Api Key")

# COMMAND ----------

# MAGIC %sql
# MAGIC show tables

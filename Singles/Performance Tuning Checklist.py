# Databricks notebook source
DBUTILS
----------
%fs ls dbfs:/user/hive/warehouse/1_demo.db/city/_delta_log/
create directory in dbfs
	dbutils.fs.mkdirs("/path/directoryname")
create file and write data to it
	dbutils.fs.put("/path/filename.txt", "content")
display file content
	dbutils.fs.head("/path/filename.txt")
list down content in a directory 
	dbutils.fs.ls("/path/")
move files from one directory to another directory 
	dbutils.fs.mv("pathl","path2")
copy file from one directory to another directory 
	dbutils.fs.cp("pathl", "path2")
remove file and directories
	dbutils.fs.rm("pathl/file.txt")		
	dbutils.fs.rm("pathl/"; True)

mount and unmount file system
	dbutils.fs.mount("mountpoint")
	dbutils.fs.unmount("mountpoint")
list down mount
	dbutils.fs.mounts()
refresh mount points
	dbutils.fs.refreshMounts()

install the packages
	dbutils.library.installPyPlC'tensorflow")
find current notebook path/ from Ul
	dbutils.notebook.getContext.notebookPath
run one notebook from another notebook
	%run path $name="rama" $location="bangalore"
	dbutils.notebook.run("path";600/{"name":"rama,V,location":"bangalore"})
exit notebook execution
	dbutils.notebook.exit("exit message")
list down secret scopes	
	dbutils.secrets.listScopes()

Others--
	dbutils.fs.put("dbfs:/FileStore/tables/text.txt", "Welcome")



dbutils.helpO
This module provides various utilities for users to interact with the rest of Databricks.
credentials: DatabricksCredentialUtils -> Utilities for interacting with credentials within notebooks
fs: DbfsUtils -> Manipulates the Databricks filesystem (DBFS) from the console
library: LibraryUtils -> Utilities for session isolated libraries
meta: MetaUtils -> Methods to hook into the compiler (EXPERIMENTAL)
notebook: NotebookUtils -> Utilities for the control flow of a notebook (EXPERIMENTAL)
preview: Preview -> Utilities under preview category
secrets: SecretUtils -> Provides utilities for leveraging secrets within notebooks
widgets: WidgetsUtils -> Methods to create and get bound value of input widgets inside notebooks

%run notebookname.ippy

==========================================================================================
Download the sampled Lending Club data
%sh mkdir -p /dbfs/tmp/sais_eu_19_demo/loans/ && wget -O /dbfs/tmp/sais_eu_19_demo/loans/SAISEU19-loan-risks.snappy.parquet https://pages.databricks.co«/rs/094-YMS-629/images/SAISEU19-loan- risks.snappy.parquet && ls -al /dbfs/tmp/sais_eu_19_demo/loans/

#Rate function for creating data
import os, shutil
import os
import shutil
from pyspark.sql.functions import * 

parquet_path = "/sais_eu_19_demo/loans_parquet"

# Delete parquet table if it exsists
if os.path.exists("/dbfs" + parquet_path):
  print("Deleting path " + parquet_path)
  shutil.rmtree("/dbfs" + parquet_path)
  
# Create a new parquet table with the parquet file
spark.read.format("parquet").load("/tmp/sais_eu_19_demo/loans/SAISEU19-loan-risks.snappy.parquet") \
  .write.format("parquet").save(parquet_path)
print("Created a Parquet table at " + parquet_path)

# Create a view on the table called loans_parquet
spark.read.format("parquet").load(parquet_path).createOrReplaceTempView("loans_parquet")
print("Defined view 'loans_parquet'")

#  %sh 
#  ls -lt /dbfs/sais_eu_19_demo/loans_delta/_delta_log

#  %sh 
#  head /dbfs/sais_eu_19_demo/loans_delta/_delta_log/00000000000000000000.json

#Commit ??
j0 = spark.read.json("/sais_eu_19_demo/loans_delta/_delta_log/00000000000000000000.json")

#  %md #### Commit Information
# Commit Information
display(j0.select("commitInfo").where("commitInfo is not null"))

#  %sh 
#  ls -lt /dbfs/sais_eu_19_demo/loans_delta/_delta_log/

#  %sh 
#  ls -lt /dbfs/sais_eu_19_demo/loans_delta/_delta_log/

READING METADATA
j0 = spark.read.json("/sais_eu_19_demo/loans_delta/_delta_log/00000000000000000000.json")

# Commit Information
display(j0.select("commitInfo").where("commitInfo is not null"))

# Add Information
display(j0.select("add").where("add is not null"))

# Add Information
display(j0.select("metadata").where("metadata is not null"))

==========================================================================================================================
EXPLAIN [ EXTENDED | CODEGEN | COST | FORMATTED ] statement
	-- Default Output (outputs physical plan
	> EXPLAIN select k, sum(v) from values (1, 2), (1, 3) t(k, v) group by k;
Cost Base Optimizer
	spark.conf.set("spark.sql.cbo.enabled", false)
CRETE TABLE
	* CREATE TABLE table_name AS SELECT * FROM another_table
	* CREATE TABLE table_name
		(field_name1 INT, field_name2 STRING)
		LOCATION '/path/to/empty/directory'
	*df.write.option("path", "/path/to/empty/directory").saveAsTable("table_name")
========================================================================================================================
SKEW
-- single column, single skew value
	SELECT /*+ SKEW(*orders', ’o_custld1, 0) */ *
	FROM orders, customers WHERE o_custId = c_custld
-- single column, multiple skew values
	SELECT /*+ SKEWCorders', 'o_custId', (0, 1, 2)) */ *
	FROM orders, customers WHERE o_custId = c_custld
-- multiple columns, multiple skew values
	SELECT /*+ SKEWC'orders',('o_custId', ' o_storeRegionId ' ), ((0, 1001), (1, 1002))) */ FROM orders, customers
	WHERE o_cust!d = c_custld AND o_storeRegion!d = c_region!d
==================================================================================================================
BROADCAST VARIABLE
==================================================





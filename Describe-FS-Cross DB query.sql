-- Databricks notebook source
use hive_metastore.1_demo



-- COMMAND ----------

show tables

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## When csv import from wizard, city table is changed to Delta table

-- COMMAND ----------

describe detail hive_metastore.1_demo.city

-- COMMAND ----------

describe extended hive_metastore.1_demo.city

-- COMMAND ----------

-- MAGIC %fs ls dbfs:/user/hive/warehouse/1_demo.db/city

-- COMMAND ----------

-- MAGIC %fs head dbfs:/user/hive/warehouse/1_demo.db/city/part-00000-cc230279-7876-430d-8e4b-159454b8c765.c000.snappy.parquet

-- COMMAND ----------

-- MAGIC %fs ls dbfs:/user/hive/warehouse/1_demo.db/city/_delta_log/

-- COMMAND ----------

-- MAGIC %fs head dbfs:/user/hive/warehouse/1_demo.db/city/_delta_log/00000000000000000001.json

-- COMMAND ----------

select count(*) from hive_metastore.dbacademy_greg_coopman_sqlcoop_com_adewd_5_1.user_lookup

-- COMMAND ----------

select a.user_id, b.cityName from hive_metastore.dbacademy_greg_coopman_sqlcoop_com_adewd_5_1.user_lookup a
cross join hive_metastore.1_demo.city b
where b.cityName = 'Hollywood'  -- case sensitive

-- COMMAND ----------

select * from hive_metastore.1_demo.city b
where b.cityName = 'Hollywood'

-- COMMAND ----------

select cast(unixtime(datecolname) as date) Salesdate
